package com.dzx.spark.web.controller;

import com.dzx.spark.web.dao.CourseClickCountDao;
import com.dzx.spark.web.entity.CourseClickCount;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DuanZhaoXu
 * @ClassName:
 * @Description:
 * @date 2019年01月24日 16:05:29
 */
@Controller
public class ImoocStatController {

    private static Map<String, String> courses = new HashMap<>();

    static {
        courses.put("112", "Spark Sql慕课网日志分析");
        courses.put("128", "10小时入门大数据");
        courses.put("145", "深度学习之神经网络核心原理与算法");
        courses.put("146", "强大的Node.js在web开发的应用");
        courses.put("131", "Vue+Django实战");
        courses.put("130", "Web前端性能优化");
    }

    @Autowired
    CourseClickCountDao courseClickCountDao;

    @RequestMapping(value = "/course_clickcount_dynamic", method = RequestMethod.POST)
    @ResponseBody
    public   List<CourseClickCount> firstDemo() throws Exception {
        //模拟假数据
//        List<CourseClickCount> list = new ArrayList<CourseClickCount>();
//        list.add(new CourseClickCount("20171111_112",1530L));
//        list.add(new CourseClickCount("20171111_128",4550L));
//        list.add(new CourseClickCount("20171111_145",6100L));
//        list.add(new CourseClickCount("20171111_146",3289L));
//        list.add(new CourseClickCount("20171111_131",7811L));
//        list.add(new CourseClickCount("20171111_130",2488L));
        //真实情况下应从Hbase中获取数据
        List<CourseClickCount> list = courseClickCountDao.query("20171111");
        for (CourseClickCount model : list) {
            model.setName(courses.get(model.getName().substring(9)));
        }
        return list;
    }

    @RequestMapping(value = "/echarts",method =RequestMethod.GET)
    public ModelAndView echarts(){
        return new ModelAndView("echarts");
    }
}
