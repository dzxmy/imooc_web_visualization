package com.dzx.spark.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author DuanZhaoXu
 * @ClassName:
 * @Description:
 * @date 2019年01月24日 11:20:04
 */
@RestController
public class HelloBoot {


    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String sayHello() {
        return "Hello World Spring Boot ...";
    }

    @RequestMapping(value = "/course_clickcount", method = RequestMethod.GET)
    public ModelAndView firstDemo(){
        return new ModelAndView("test");
     }
}
