package com.dzx.spark.web.entity;

/**
 * @author DuanZhaoXu
 * @ClassName:
 * @Description:
 * @date 2019年01月24日 15:57:30
 */

public class CourseClickCount {


    private  String name;
    private  long value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long  value) {
        this.value = value;
    }


    public CourseClickCount(String name, Long value) {
        this.name = name;
        this.value = value;
    }

    public CourseClickCount() {
    }


    @Override
    public String toString() {
        return "CourseClickCount{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
