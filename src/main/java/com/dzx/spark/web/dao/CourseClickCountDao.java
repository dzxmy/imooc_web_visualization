package com.dzx.spark.web.dao;

import com.dzx.spark.web.entity.CourseClickCount;
import com.dzx.spark.web.utils.HBaseUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author DuanZhaoXu
 * @ClassName:
 * @Description:
 * @date 2019年01月24日 15:58:48
 */
@Repository
public class CourseClickCountDao {


    private static final String  tableName = "imooc_course_clickcount";

    /**
     * 根据天查询
     * @param day
     * @return
     * @throws Exception
     */
    public List<CourseClickCount> query(String day) throws  Exception{
        List<CourseClickCount> list  =new ArrayList<>();
        //去hbase表中根据day获取实战课程访问量
        Map<String, Long> map = HBaseUtils.getInstance().query(tableName, day);
        for(Map.Entry<String,Long> entry:map.entrySet()){
            CourseClickCount model = new CourseClickCount();
            model.setName(entry.getKey());
            model.setValue(entry.getValue());
            list.add(model);
        }
         return list;
    }


}
